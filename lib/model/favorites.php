<?php

namespace Project\Favorites\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main,
    Project\Favorites\Config;

class FavoritesTable extends DataManager {

    public static function tableCreate() {
        static::getEntity()->getConnection()->query("CREATE TABLE " . self::getTableName() . " (
            ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            ELEMENT_ID INT,
            USER_ID INT,
            TYPE VARCHAR(50)
        );");
    }

    public static function tableDrop() {
        if (Config::IS_FROP_TABLE) {
            static::getEntity()->getConnection()->query("DROP TABLE IF EXISTS " . self::getTableName() . ";");
        }
    }

    public static function getTableName() {
        return 'project_favorits';
    }

    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('ELEMENT_ID'),
            new Main\Entity\IntegerField('USER_ID'),
            new Main\Entity\StringField('TYPE'),
        );
    }

}

<?php

use Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Favorites\Model\FavoritesTable;

IncludeModuleLangFile(__FILE__);

class project_favorites extends CModule {

    public $MODULE_ID = 'project.favorites';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_REDIRECT_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_REDIRECT_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_REDIRECT_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallDB();
        $this->InstallFiles();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallDB();
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /*
     * InstallDB
     */

    public function InstallDB() {
        FavoritesTable::tableCreate();
    }

    public function UnInstallDB() {
        FavoritesTable::tableDrop();
    }

    /*
     * InstallFiles
     */

    public function InstallFiles($arParams = array()) {
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/project.favorites/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/project.favorites/' . $this->MODULE_ID . '/', true, true);
    }

    public function UnInstallFiles() {
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/project.favorites/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/project.favorites/' . $this->MODULE_ID . '/'); //css
    }


}

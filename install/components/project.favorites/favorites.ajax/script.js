(function (window) {

    if (window.jsPortalFavorites) {
        return;
    }

    window.jsPortalFavorites = function (arParams, togge) {
        var self = this;
        self.config = {
            ajax: arParams.AJAX,
            select: arParams.SELECT+'.element'+arParams.ELEMENT_ID,
            infavorites: arParams.INFAVORITES,
        };
        self.param = {
            TYPE: arParams.TYPE,
            ELEMENT_ID: arParams.ELEMENT_ID,
        };
        $(document).on('click', self.config.select, function () {
            if(portalIsAuthCall()) {
                self.param.ADD = $(this).is('.added') ? 0 : 1;
                $.get(self.config.ajax, self.param, function (data) {});
                togge($(self.config.select));
            }
        });
        if(self.config.infavorites) {
            togge($(self.config.select));
        }
    };
})(window);
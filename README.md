# Избранное на сайте #

необходим модуль https://bitbucket.org/project-tm/project.core

Добавить в избранное
```
#!php
<a class="add-to-favorites element<?= $arResult['ID'] ?>">Добавить в избранное</a>
$APPLICATION->IncludeComponent(
        "project.favorites:favorites.ajax", "game", array(
    'TYPE' => 'GAME',
    'SELECT' => '.game-main-info.game .liked .add-to-favorites',
    'ELEMENT_ID' => $ElementID,
        )
);

```

Список

```
#!php
$APPLICATION->IncludeComponent("project.favorites:favorites.ajax.list", "game", array(
    'TYPE' => 'GAME',
        )
);
```